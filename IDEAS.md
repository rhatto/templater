# Ideas

* Bazaar:
  * README with basic info and HOWTO
  * Rename de project to "boilerplater" or "skeletor"?
* Licenses:
  * Public domain
  * Creative Commons
  * Perl
  * [Unlicense](https://unlicense.org/)
  * Add [Software Package Data Exchange (SPDX)](https://spdx.org) ([about](https://lwn.net/Articles/739183/)) into files
* Metafiles:
  * Changelog formats
  * Semver
  * [Developer Certificate of Origin](https://developercertificate.org/)
    ([1](https://elinux.org/Developer_Certificate_Of_Origin),
     [2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/submitting-patches.rst).
* Standards:
  * Robotstxt
  * Securitytxt
* Contributing:
  * [Wrangling Web Contributions: How to Build a CONTRIBUTING.md](https://mozillascience.github.io/working-open-workshop/contributing/)
  * [Good-CONTRIBUTING.md-template.md · GitHub](https://gist.github.com/PurpleBooth/b24679402957c63ec426)
  * [vue/CONTRIBUTING.md at dev · vuejs/vue · GitHub](https://github.com/vuejs/vue/blob/dev/.github/CONTRIBUTING.md)
  * More Code of Conduct flavors
* Modules:
  * Python
  * Flask
  * Debian package
  * Website checklist
  * Manpage
  * Multilicense
  * Bugs-everywhere
  * Bookdown
  * Mkdocs:
    * Runs `mkdocs new`.
    * Sets a Makefile and script for watch mode (inotify).
* Code:
  * Per-module push-to-deploy scripts (`bin/deploy.d`)
  * Makefiles: non-conflicting target names with aliases/shorthands in the main Makefile
  * Check/detect/verify mode, telling which modules are installed and checking for missing things (like LICENSE) or using a .templaterrc
  * Update mode to update configs between a project and templater's defaults
  * If git is in use, commit all changes, but check first it there are non-commited changes in the repo
  * Hugo: tell the user about adding a theme: http://gohugo.io/getting-started/quick-start/
  * Be verbose about the need to review and edit files, adding LICENSE headers into source files, etc
  * Try an alternative command-line format, like "module:option1=value,option2=value with spaces"
  * Git: optionally configure user.signingkey, commit.gpgsign, etc
  * Debian package module
