Templater / Skeletor / Boilerplater
===================================

Project bootstrapping made easy! Setup useful configuration for your software
project.

Tired of tediously creating the basic skeleton for your projects? What if you
forget to add a LICENSE or some other important file? This project helps you
handle metaproject tasks.

## Instalation

Simply clone it and add to your `$PATH`:

    git clone https://git.fluxo.info/templater

You can also verify the latest commit's OpenPGP signature:

    /usr/bin/git -C templater verify-commit HEAD

Note that `/usr/bin/git` is called to avoid any other `git` wrappers or aliases
you might have available on your shell.

## Examples

### Basic project

Say you want to start a new project `myproject` at `$HOME/src/myproject`, using git, basic
files an a GPLv3 license:

    templater $HOME/src/myproject basic gpl git

### Drupal 8 Development

Create the project (requires [KVMX](https://kvmx.fluxo.info)):

    templater drupal8 basic gpl conduct git puppet kvmx drupal8 && cd drupal8

Basic puppet configuration:

    vim puppet/config/node/drupal8.example.org.yaml # set nodo::role to 'dev::drupal8'

Edit your drupal config

    vim settings.php # database config, trusted_host_patterns, etc

Set kvmx parameters:

    kvmx edit # basebox shared_folder_mountpoint provision_rsync provision_command run_spice_client port_mapping hostname

Provision the virtual machine:

    kvmx provision

Install the drupal instance:

    echo '( cd /srv/shared && make drush        )' | kvmx ssh
    echo '( cd /srv/shared && make site_install )' | kvmx ssh

## Writing modules

Modules can be written using any programming language. It will be called from the project
toplevel folder with templater's `share` folder as the first parameter.

Module layout:

- description: states in natural language what the modules does
- setup: states in programing language what the module does (the actual script)
- files (optional)

Example: when you call `templater myproject basic`, templater finds a script
at `$path_to_templater_install/share/basic/setup` and execs it like this:

    cd myproject && share/basic/setup $path_to_templater_install/share/templater

You might even use `templater` to create a module:

    cd path/to/templater/repo && templater share/templater/<module_name> templater
