#
# Makefile for a Drupal 7 Website
#

# Parameters
CORE      = 7
PROJECT   = drupal
ENV       = dev
DATE      = $(shell date +%Y%m%d)
CWD       = $(shell pwd)
FEATURE   = drupal_feature
DESTDIR  ?= /var/www/data
DRUPAL    = $(DESTDIR)/drupal-$(CORE)
DRUSH     = drush -r $(DRUPAL) -l $(PROJECT)

# Setup production environment
production: submodules post_receive drush settings ownership

# Setup a development environment
develop: submodules $(CONTAINER)
	#rm -rf $(DRUPAL)/sites/default
	#ln -s $(PROJECT) $(DRUPAL)/sites/default

# Old target
# Install the platform
#platform: drupal drush settings symlinks initdb cache

# Keep code in sync
sync-code: update drush cache

# Initialize and update git submodules
submodules:
	git submodule sync          --recursive
	git submodule update --init --recursive

# Setup virtual machine using vagrant
vagrant:
	vagrant up || true
	vagrant provision
	echo '( cd /vagrant && make drush )'    | vagrant ssh
	echo '( cd /vagrant && make settings )' | vagrant ssh
	#vagrant ssh -c "$(DRUSH) pm-enable devel -y"

# Setup virtual machine using kvmx
kvmx:
	#kvmx up || true
	kvmx provision
	echo '( cd /srv/shared && make drush )'    | kvmx ssh
	echo '( cd /srv/shared && make settings )' | kvmx ssh
	#kvmx ssh "$(DRUSH) pm-enable devel -y"

# Old target
# Download drupal
#drupal:
#	drush pm-download drupal-$(CORE) --destination=$(DESTDIR)
#	( cd $(DRUPAL)/.. && ln -s drupal-$(CORE).* drupal-$(CORE) )

# Old target
# Make the needed symlinks
#symlinks:
#	ln -s -t $(DRUPAL)/sites/$(PROJECT) $(CWD)/files
#	ln -s -t $(DRUPAL)/sites/$(PROJECT) $(CWD)/themes
#	ln -s -t $(DRUPAL)/sites/$(PROJECT) $(CWD)/modules
#	ln -s -t $(DRUPAL)/sites/$(PROJECT) $(CWD)/libraries

# Default settings
default_settings:
	test -s settings.dev.php || cp $(DRUPAL)/sites/default/default.settings.php settings.dev.php

# Custom settings
custom_settings:
	#mkdir -p  $(DRUPAL)/sites/$(PROJECT)
	#test -s   $(DRUPAL)/sites/$(PROJECT)/settings.$(ENV).php || cp settings.dev.php       $(DRUPAL)/sites/$(PROJECT)/settings.$(ENV).php
	#test -s   $(DRUPAL)/sites/$(PROJECT)/settings.php        || ln -s settings.$(ENV).php $(DRUPAL)/sites/$(PROJECT)/settings.php
	#chmod 640 $(DRUPAL)/sites/$(PROJECT)/settings.php
	#sudo chown drupal.$(PROJECT) settings.$(ENV).php
	test -s   settings.$(ENV).php || cp settings.dev.php       settings.$(ENV).php
	test -s   settings.php        || ln -s settings.$(ENV).php settings.php
	chmod 640 settings.php

# Setup drupal settings
settings: default_settings custom_settings

# Fix ownership
ownership:
	sudo chown -R $(PROJECT). $(DRUPAL)/sites/$(PROJECT)

# Run drush makefile
drush:
	sudo drush make --no-core drupal.make.yml $(DRUPAL)

# Run drush site-install
site_install: settings
	$(DRUSH) site-install -y

# Run drush site-install, sqlite version
# See https://api.drupal.org/api/drupal/core%21INSTALL.sqlite.txt/8.4.x
site_install_sqlite: settings
	mkdir -p sql/.ht.sqlite
	touch sql/dumps/.ht.sqlite
	$(DRUSH) site-install --db-url=sqlite://$(CWD)/sql/dumps/.ht.sqlite

# Update the database
updatedb:
	$(DRUSH) updatedb -y

# Load the database dump, clear the drupal cache and fix image paths
# See https://www.drupal.org/node/628130
initdb: settings
	test -s sql/dumps/latest.sql.gz && gzip -dc sql/dumps/latest.sql.gz | $(DRUSH) sql-cli || true
	test -s sql/dumps/latest.sql.gz && $(DRUSH) updatedb -y || true
	echo "TRUNCATE TABLE cache" | $(DRUSH) sql-cli
	$(DRUSH) cc all
	$(DRUSH) updatedb -y

# Dump the database, clearing the cache to ensure a small package
dumpdb: settings cache
	mkdir -p sql/dumps
	rm -f sql/dumps/latest.sql.gz
	rm -f sql/dumps/$(DATE).sql.gz
	$(DRUSH) sql-dump > sql/dumps/$(DATE).sql
	( cd sql/dumps && gzip $(DATE).sql && ln -s $(DATE).sql.gz latest.sql.gz )

# Clear drupal cache
cache:
	$(DRUSH) cc all

# Destroy the database
destroydb: settings
	echo "DROP DATABASE $(PROJECT); CREATE DATABASE $(PROJECT);" | $(DRUSH) sql-cli

# Reinitializes the database
reinitdb: destroydb initdb

# Cleanup develop environment
clean:
	vagrant halt       || true
	vagrant destroy -f || true
	kvmx poweroff      || true
	kvmx destroy       || true
	#rm -rf vendor/drupal*

# Export configuration
export:
	$(DRUSH) features-update $(FEATURE) -y

# Import configuration
import:
	$(DRUSH) features-revert $(FEATURE) -y

# Diff configuration
diff:
	$(DRUSH) features-diff $(FEATURE)

# Update the codebase
update:
	git pull
	git submodule update --init

# Upgrade modules
upgrade-modules:
	sudo $(DRUSH) up

# Upgrade drupal
upgrade-drupal:
	BASE=$(DESTDIR) sudo drupal upgrade $(CORE)

# Pull changes
pull:
	git pull

# Reset the working copy
reset:
	#git reset HEAD
	git checkout -f

# Fix local folder permission
perms:
	chmod 755 .

# Deploy code pushed on remote host
deploy: perms reset submodules
	drush cc all
	drush features-revert -y $(FEATURE)

sync_files_from_development:
	rsync -avz --delete $(DEVELOPMENT)/sql/dumps/ sql/dumps/
	rsync -avz --delete $(DEVELOPMENT)/files/     files/

sync_files_from_production:
	rsync -avz --delete $(PRODUCTION)/sql/dumps/ sql/dumps/
	rsync -avz --delete $(PRODUCTION)/files/     files/

sync_files_to_development:
	rsync -avz --delete sql/dumps/ $(DEVELOPMENT)/sql/dumps/
	rsync -avz --delete files/     $(DEVELOPMENT)/files/

sync_files_to_production:
	rsync -avz --delete sql/dumps/ $(PRODUCTION)/sql/dumps/
	rsync -avz --delete files/     $(PRODUCTION)/files/
