#
# Makefile for reveal.js presentation
#

# See https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides
# Also look for variables at /usr/share/pandoc/data/templates/default.revealjs
REVEAL_THEME=moon

web:
	test -s .gitmodules && git submodule update --init --recursive
	pandoc -V theme=$(REVEAL_THEME) -V css=index.css -s --mathjax -i -t revealjs index.md -o index.html
	pandoc -t beamer index.md -o index.pdf 
