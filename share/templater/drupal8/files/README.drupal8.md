Drupal 8 Application HOWTO
==========================

This is the basic worflow for a Drupal 8 project set using
[templater](https://templater.fluxo.info).

Requirements
------------

Basique requirements are:

* A [make](https://packages.debian.org/stable/make) implementation.
* Container manager like vagrant, docker or [kvmx](https://kvmx.fluxo.info).

Copying files
-------------

Put files and database dumps in the following folders:

    rsync -avz --delete /path/to/backups/files/ files/
    rsync -avz --delete /path/to/backups/sql/   sql/

Check the Makefile.drupal8 for the db dump file name and format. Commands like
the examples above can be used to transport files from production back to the
development trees.

Development
-----------

Create the development environment using

    make develop

You might need to setup vagrant, docker or kvmx first.

## Default site installation

If you need a fresh drupal database installation, do this in the project
folder inside the container:

    make site_install

## Loading and reloading the database

To load or reload the database from the latest dump available at the `sql/`
folder, do the following in the project folder and inside the container:

    make reinitdb

## Acessing

The exact port dependes on what you configured on your container configuration,
but you should be able to reach your drupal installation at an address like

    http://localhost:8080

## Exporting configuration

Export the configuration is easy. Do the following in the project folder and
inside your container:

    make export

## Cleaning

Cleaning is simple and can be run directly outside the container:

    make clean

Then the project is ready to be provisioned again into a container.

Production
----------

Prodution provisioning example:

    make production                                 # setup environment
    sudo vim settings.prod.php                      # add db info
    sudo vim services.prod.yml                      # customize
    sudo vim /var/www/data/drupal-8/sites/sites.php # add site into the pool
    make initdb                                     # initialize db from a dump
