<?php

// @codingStandardsIgnoreFile

/**
 * Database settings:
 *
 * The $databases array specifies the database connection or
 * connections that Drupal may use.  Drupal is able to connect
 * to multiple databases, including multiple types of databases,
 * during the same request.
 *
 * One example of the simplest connection array is shown below. To use the
 * sample settings, copy and uncomment the code below between the @code and
 * @endcode lines and paste it after the $databases declaration. You will need
 * to replace the database username and password and possibly the host and port
 * with the appropriate credentials for your database system.
 *
 * The next section describes how to customize the $databases array for more
 * specific needs.
 *
 * @code
 * $databases['default']['default'] = [
 *   'database' => 'databasename',
 *   'username' => 'sqlusername',
 *   'password' => 'sqlpassword',
 *   'host' => 'localhost',
 *   'port' => '3306',
 *   'driver' => 'mysql',
 *   'prefix' => '',
 *   'collation' => 'utf8mb4_general_ci',
 * ];
 * @endcode
 */
/**
$databases['default']['default'] = array (
  'database' => 'site',
  'username' => 'site',
  'password' => 'hackme',
  'prefix' => '',
  'host' => 'localhost',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
 */

/**
 * Development.
 *
 * Disable caching during development
 * See https://www.drupal.org/node/2598914
 */
//$settings['container_yamls'][]                   = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['container_yamls'][]                     = DRUPAL_ROOT . '/sites/default/services.dev.yml';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess']  = FALSE;
$settings['cache']['bins']['render']               = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache']   = 'cache.backend.null';
